﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Collage.UI.Constants;

namespace Collage.UI.Helpers
{
    public static class BitmapSourceHelper
    {
        public static BitmapSource LoadFromFile(string filename)
        {
            return new BitmapImage(new Uri(filename));
        }

        public static Bitmap ToBitmap(this BitmapSource bitmapSource)
        {
            // TODO: bad_fix возникает исключение в случае bitmapSource.ToBitmap().ToBitmapImage();
            //using ()
            //{
            var memoryStream = new MemoryStream();
            BitmapEncoder bitmapEncoder = new BmpBitmapEncoder();

            bitmapEncoder.Frames.Add(BitmapFrame.Create(bitmapSource));
            bitmapEncoder.Save(memoryStream);
            return new Bitmap(memoryStream);
            //}
        }

        public static BitmapSource ToDpi96(this BitmapSource bitmapSource)
        {
            const double dpi = 96;
            var width = bitmapSource.PixelWidth;
            var height = bitmapSource.PixelHeight;

            var stride = width * 4; // 4 bytes per pixel
            var pixelData = new byte[stride * height];
            bitmapSource.CopyPixels(pixelData, stride, 0);

            return BitmapSource.Create(UiConstants.Width, UiConstants.Height, dpi, dpi, PixelFormats.Bgra32, null, pixelData, stride);
        }
    }
}
