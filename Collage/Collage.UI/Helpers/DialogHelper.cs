﻿using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;
using SaveFileDialog = Microsoft.Win32.SaveFileDialog;

namespace Collage.UI.Helpers
{
    public static class DialogHelper
    {
        public static string LoadImage()
        {
            var dialog = new OpenFileDialog
            {
                DefaultExt = ".png",
                Filter = "PNG Files (*.png)|*.png|JPEG Files (*.jpeg)|*.jpeg|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif"
            };

            var result = dialog.ShowDialog();

            return result.HasValue && result.Value ? dialog.FileName : string.Empty;
        }

        //public static void SaveImage(Image image)
        //{
        //    var dialog = new SaveFileDialog
        //    {
        //        Filter = "PNG file (*.png)|*.png|All files (*.*)|*.*",
        //        RestoreDirectory = true
        //    };

        //    var result = dialog.ShowDialog();

        //    if (result.HasValue && result.Value)
        //    {
        //        using (var stream = dialog.OpenFile())
        //        {
        //            var renderTargetBitmap = new RenderTargetBitmap((int)image.Source.Width, (int)image.Source.Height, 96, 96, PixelFormats.Default);
        //            renderTargetBitmap.Render(image);
        //            var jpegBitmapEncoder = new PngBitmapEncoder();
        //            jpegBitmapEncoder.Frames.Add(BitmapFrame.Create(renderTargetBitmap));
        //            jpegBitmapEncoder.Save(stream);
        //            stream.Flush();
        //        }
        //    }
        //}
    }
}
