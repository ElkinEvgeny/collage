﻿using System.Drawing;
using System.Windows.Media;
using Image = System.Windows.Controls.Image;

namespace Collage.UI.Helpers
{
    public static class ImageHelper
    {
        public static Image Create(int width, int height, Bitmap bitmap)
        {
            return new Image 
            {
                Width = width,
                Height = height,
                Stretch = Stretch.Fill,
                Source = bitmap.ToBitmapImage()
            };
        }
    }
}
