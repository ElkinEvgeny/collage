﻿using System.Drawing;
using System.IO;
using System.Windows.Media.Imaging;

namespace Collage.UI.Helpers
{
    public static class BitmapHelper
    {
        public static BitmapImage ToBitmapImage(this Bitmap @this)
        {
            using (var memory = new MemoryStream())
            {
                @this.Save(memory, System.Drawing.Imaging.ImageFormat.Png);

                memory.Position = 0;
                var bitmapimage = new BitmapImage();

                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }

        // ReSharper disable once InconsistentNaming
        public static Bitmap GetROI(this Bitmap @this, int x, int y, int width, int height)
        {
            var result = new Bitmap(width, height);

            for (var i = 0; i < width; i++)
            {
                for (var j = 0; j < height; j++)
                {
                    var color = @this.GetPixel(x + i, y + j);
                    result.SetPixel(i, j, color);
                }
            }

            return result;
        }

        public static Bitmap Snip(this Bitmap @this)
        {
            var width = @this.Width / 64 * 64;
            var height = @this.Height / 64 * 64;

            var offsetX = (@this.Width - width) / 2;
            var offsetY = (@this.Height - height) / 2;

            return @this.GetROI(offsetX, offsetY, width, height);
        }
    }
}