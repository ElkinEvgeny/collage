﻿namespace Collage.UI.Constants
{
    public static class UiConstants
    {
        public const int Width = 512;
        public const int Height = 512;
    }
}
