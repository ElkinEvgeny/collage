﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Collage.Core.CollageModule.Interfaces;
using Collage.Core.CollageModule.Services;
using Collage.Core.Helpers;
using Collage.Core.SegmentModule.Interfaces;
using Collage.Core.SegmentModule.Services;
using Collage.UI.Helpers;

namespace Collage.UI
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly ISegmentService _segmentService;
        private readonly ICollageService _collageService;

        public MainWindow()
        {
            InitializeComponent();
            _segmentService = new SegmentService();
            _collageService = new CollageService();
        }

        private void LoadImageClick(object sender, RoutedEventArgs e)
        {
            var image = new Image();

            var filename = DialogHelper.LoadImage();
            if (!string.IsNullOrEmpty(filename))
            {
                Canvas.Children.Clear();

                var bitmapSource = BitmapSourceHelper.LoadFromFile(filename);
                image.Source = bitmapSource.ToBitmap().Snip().ToBitmapImage();
                Canvas.Children.Add(image);

                Canvas.Width = image.Source.Width;
                Canvas.Height = image.Source.Height;
            }
        }

        private void MakeCollageClick(object sender, RoutedEventArgs e)
        {
            if (!HasBackground())
            {
                return;
            }

            ClearCanvas();

            var backgroundBitmapImage = GetBackground();
            var backgroundBitmap = backgroundBitmapImage.ToBitmap();

            var segments = _segmentService.GetSegments(backgroundBitmap.Width, backgroundBitmap.Height);
            segments = segments.Shuffle();

            var dispatcher = Dispatcher.CurrentDispatcher;

            var segmentBitmaps = segments.Select(i => new
            {
                Segment = i,
                Bitmap = backgroundBitmap.GetROI(i.Left, i.Top, i.Width, i.Height)
            });

            foreach (var segmentBitmap in segmentBitmaps)
            {
                Task.Factory.StartNew(() =>
                    {
                        var segment = segmentBitmap.Segment;

                        var mark = _collageService.GetMark(segmentBitmap.Bitmap);
                        var bitmap = mark == null
                            ? segmentBitmap.Bitmap
                            : _collageService.GetSegment(mark);

                        dispatcher.Invoke(() =>
                            {
                                var image = ImageHelper.Create(segment.Width, segment.Height, bitmap);

                                Canvas.Children.Add(image);
                                Canvas.SetTop(image, segment.Top);
                                Canvas.SetLeft(image, segment.Left);
                            });
                    });
            }
        }

        private void ClearCanvas()
        {
            var background = Canvas.Children[0];
            Canvas.Children.Clear();
            Canvas.Children.Add(background);
        }

        private bool HasBackground()
        {
            return Canvas.Children.Count > 0;
        }

        private BitmapSource GetBackground()
        {
            var background = Canvas.Children[0] as Image;
            var bitmapImage = background.Source as BitmapImage;
            return bitmapImage;
        }
    }
}
