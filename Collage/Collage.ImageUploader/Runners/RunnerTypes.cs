﻿using System;
using Collage.ImageUploader.Interfaces;

namespace Collage.ImageUploader.Runners
{
    public enum RunnerTypes
    {
        Classic, 
        Parallel
    }
}
