﻿using System;
using System.Threading.Tasks;
using Collage.ImageUploader.Interfaces;

namespace Collage.ImageUploader.Runners
{
    public class ParallelRunner : IRunner
    {
        public void Run(int start, int end, Action<int> action)
        {
            Parallel.For(start, end, action);
        }
    }
}