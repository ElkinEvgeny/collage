﻿using System;

namespace Collage.ImageUploader.Runners
{
    public static class Runner
    {
        public static void Run(RunnerTypes type, int start, int end, Action<int> action)
        {
            switch (type)
            {
                case RunnerTypes.Classic:
                    new ClassicRunner().Run(start, end, action);
                    break;
                case RunnerTypes.Parallel:
                    new ParallelRunner().Run(start, end, action);
                    break;
                default:
                    throw new NotSupportedException(nameof(type));
            }
        }
    }
}