﻿using System;
using Collage.ImageUploader.Interfaces;

namespace Collage.ImageUploader.Runners
{
    public class ClassicRunner : IRunner
    {
        public void Run(int start, int end, Action<int> action)
        {
            for (var i = start; i < end; i++)
            {
                action(i);
            }
        }
    }
}
