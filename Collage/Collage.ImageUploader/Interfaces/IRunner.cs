﻿using System;

namespace Collage.ImageUploader.Interfaces
{
    public interface IRunner
    {
        void Run(int start, int end, Action<int> action);
    }
}