﻿namespace Collage.ImageUploader.Interfaces
{
    interface IProcessor
    {
        string Name { get; }
        void Run();
    }
}
