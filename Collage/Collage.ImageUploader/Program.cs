﻿using System;
using Collage.ImageUploader.Interfaces;
using Collage.ImageUploader.Options;
using Collage.ImageUploader.Processors;
using CommandLine;
using CommandLine.Text;

namespace Collage.ImageUploader
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var parser = Parser.Default.ParseArguments<UploadOption, CompressOption, CalculateOption, ElasticOption>(args);

            var processor = parser.MapResult<UploadOption, CompressOption, CalculateOption, ElasticOption, IProcessor>(
                uploadOption => new UploadProcessor(uploadOption),
                compressOption => new CompressProcessor(compressOption),
                calculateOption => new CalculateProcessor(calculateOption),
                elasticOption => new ElasticProcessor(elasticOption),
                error => null as IProcessor);

            if (processor == null)
            {
                HelpText.AutoBuild(parser);
                return;
            }
            Console.WriteLine("Run {0}:", processor.Name);
            processor.Run();
            Console.WriteLine("Complete");
            Console.ReadKey();
        }
    }
}
