﻿using Collage.ImageUploader.Runners;
using CommandLine;

namespace Collage.ImageUploader.Options
{
    [Verb("compress")]
    public class CompressOption
    {
        [Option('d', "directory", Required = true)]
        public int DirectoryPath { get; set; }

        [Option('o', "offset", Required = false, Default = 0)]
        public int Offset { get; set; }

        [Option('r', "runner", Required = false, Default = RunnerTypes.Classic)]
        public RunnerTypes RunnerType { get; set; }
    }
}