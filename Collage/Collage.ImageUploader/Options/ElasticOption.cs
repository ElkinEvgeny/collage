﻿using Collage.ImageUploader.Runners;
using CommandLine;

namespace Collage.ImageUploader.Options
{
    [Verb("elastic")]
    public class ElasticOption
    {
        [Option('d', "directory", Required = true)]
        public int DirectoryPath { get; set; }

        [Option('r', "runner", Required = false, Default = RunnerTypes.Classic)]
        public RunnerTypes RunnerType { get; set; }
    }
}