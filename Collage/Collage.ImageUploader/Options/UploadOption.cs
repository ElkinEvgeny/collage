﻿using CommandLine;
using Collage.ImageUploader.Runners;

namespace Collage.ImageUploader.Options
{
    [Verb("upload")]
    public class UploadOption
    {
        [Option('s', "source", Required = true)]
        public int SourceFileName { get; set; }

        [Option('o', "offset", Required = false, Default = 0)]
        public int Offset { get; set; }

        [Option('r', "runner", Required = false, Default = RunnerTypes.Classic)]
        public RunnerTypes RunnerType { get; set; }
    }
}