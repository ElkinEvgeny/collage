﻿using Collage.ImageUploader.Runners;
using CommandLine;

namespace Collage.ImageUploader.Options
{
    [Verb("calculate")]
    public class CalculateOption
    {
        [Option('d', "directory", Required = true)]
        public int Directory { get; set; }

        [Option('o', "offset", Required = false, Default = 0)]
        public int Offset { get; set; }

        [Option('r', "runner", Required = false, Default = RunnerTypes.Classic)]
        public RunnerTypes RunnerType { get; set; }
    }
}
