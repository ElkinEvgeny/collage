﻿using System;
using System.IO;
using Collage.ImageUploader.Configs;
using Collage.ImageUploader.Options;
using Nest;
using Newtonsoft.Json;
using IProcessor = Collage.ImageUploader.Interfaces.IProcessor;

namespace Collage.ImageUploader.Processors
{
    public class ElasticProcessor : IProcessor
    {
        private readonly ElasticOption _option;
        private readonly ElasticConfig _config;

        public ElasticProcessor(ElasticOption option)
        {
            _option = option;
            _config = ConfigManager.GetConfig<ElasticConfig>();
        }

        public string Name => "elastic";

        public void Run()
        {
            var uri = new Uri(_config.ElasticUrl);
            var client = new ElasticClient(uri);
            var completed = false;

            var bulkAllObserver = new BulkAllObserver(
                response =>
                {
                    completed = true;
                }, exception =>
                {
                    completed = true;
                },
                () =>
                {
                    completed = true;
                });

            var files = Directory.GetFiles($"{_config.InputFilesPath}/{_option.DirectoryPath}");
            foreach (var file in files)
            {
                var textData = File.ReadAllText(file);
                var datas = JsonConvert.DeserializeObject<PhotoCharacteristics[]>(textData);
                var r = client.BulkAll(datas, x => x.Index(_config.Index))
                    .Subscribe(bulkAllObserver);
                while (!completed)
                { }
            }
        }
    }
}