﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using Collage.ImageUploader.Configs;
using Collage.ImageUploader.Interfaces;
using Collage.ImageUploader.Options;
using Collage.ImageUploader.Runners;

namespace Collage.ImageUploader.Processors
{
    public class UploadProcessor : IProcessor
    {
        public string Name => "uploading";

        private readonly UploadOption _option;
        private readonly UploadConfig _config;

        private static string[] _urls;
        private static string _targetFilesTemplate;

        public UploadProcessor(UploadOption option)
        {
            _option = option;
            _config = ConfigManager.GetConfig<UploadConfig>();
        }
        
        public void Run()
        {
            _urls = File.ReadLines($"{_config.InputFilesPath}/{_option.SourceFileName}.txt").ToArray();
            var directoryName = $"{_config.TargetFilesPath}/{_option.SourceFileName}";
            if (!Directory.Exists(directoryName))
            {
                Directory.CreateDirectory(directoryName);
            }
            _targetFilesTemplate = directoryName  + "/{0}.jpg";

            Runner.Run(_option.RunnerType, _option.Offset, _urls.Length, UploadImage);
        }

        private static void UploadImage(int i)
        {
            try
            {
                var url = _urls[i];
                if (string.IsNullOrEmpty(_urls[i]))
                {
                    return;
                }
                byte[] data;
                using (var webClient = new HttpClient())
                {
                    data = webClient.GetByteArrayAsync(new Uri(url)).Result;
                }
                File.WriteAllBytes(string.Format(_targetFilesTemplate, i), data);
            }
            catch (Exception)
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("Error on index: {0}", i);
                Console.ResetColor();
            }
        }
    }
}
