﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Collage.ImageUploader.Configs;
using Collage.ImageUploader.Interfaces;
using Collage.ImageUploader.Options;
using Collage.ImageUploader.Runners;
using Collage.Core.Helpers;
using Newtonsoft.Json;

namespace Collage.ImageUploader.Processors
{
    public class CalculateProcessor : IProcessor
    {
        public string Name => "calculating";

        private readonly CalculateOption _option;
        private readonly CalculateConfig _config;

        private static string[] _directories;
        private static string _targetDirectoryTemplate;
        private static string _sourceDirectoryTemplate;

        public CalculateProcessor(CalculateOption option)
        {
            _option = option;
            _config = ConfigManager.GetConfig<CalculateConfig>();
        }

        public void Run()
        {
            var path = $"{_config.InputFilesPath}/{_option.Directory}";
            _directories = Directory.GetDirectories(path);
            _targetDirectoryTemplate = $"{_config.TargetFilesPath}/{_option.Directory}/{{0}}.txt";
            _sourceDirectoryTemplate = $"{_option.Directory}/{{0}}/{{1}}";


            Runner.Run(_option.RunnerType, _option.Offset, _directories.Length, CalculateImages);
        }

        public void CalculateImages(int index)
        {
            var files = Directory.GetFiles(_directories[index]).ToArray();
            var directoryName = Path.GetFileName(_directories[index]);
            using (var sw = new StreamWriter(string.Format(_targetDirectoryTemplate, directoryName)))
            {
                var listPhotos = new List<PhotoCharacteristics>();
                foreach (var file in files)
                {
                    var metrics = PhotoProcessingHelper.CalculateMetrics(file, 4);
                    var claster = PhotoProcessingHelper.CalculateClaster(file);
                    listPhotos.Add(new PhotoCharacteristics()   
                    {
                        Claster = claster,
                        Metrics = metrics.ToArray(),
                        Path = string.Format(_sourceDirectoryTemplate, directoryName, Path.GetFileName(file))
                    });
                }
                var json = JsonConvert.SerializeObject(listPhotos);
                sw.Write(json);
            }
        }
    }
    
    public class PhotoCharacteristics
    {
        public int Claster { get; set; }
        
        public int[] Metrics { get; set; }

        public string Path { get; set; }
    }
}
