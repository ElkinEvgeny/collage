﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using Collage.ImageUploader.Configs;
using Collage.ImageUploader.Interfaces;
using Collage.ImageUploader.Options;
using Collage.ImageUploader.Runners;
using ImageProcessor;

namespace Collage.ImageUploader.Processors
{
    public class CompressProcessor : IProcessor
    {
        public string Name => "uploading";

        private readonly CompressOption _option;
        private readonly CompressConfig _config;

        private static string[] _files;
        private static string _path;
        private static string _targetFilesTemplate;

        public CompressProcessor(CompressOption option)
        {
            _option = option;
            _config = ConfigManager.GetConfig<CompressConfig>();
        }
        
        public void Run()
        {
            _path = $"{_config.InputFilesPath}/{_option.DirectoryPath}";
            _files = Directory.GetFiles(_path).ToArray();
            var directoryPath = $"{_config.TargetFilesPath}/{_option.DirectoryPath}";
            Directory.CreateDirectory(directoryPath);

            var currentFolder = 0;
            while (_files.Length - currentFolder * 1000 > 0)
            {
                _targetFilesTemplate = $"{directoryPath}/{currentFolder}/{{0}}{{1}}.jpg";
                Directory.CreateDirectory($"{directoryPath}/{currentFolder}");
                Runner.Run(_option.RunnerType, currentFolder * 1000, currentFolder * 1000 + Math.Min(1000, _files.Length - currentFolder * 1000), CompressImage);
                currentFolder++;
            }
        }

        public static void CompressImage(int index)
        {
            {
                var path = Path.Combine(_path, _files[index]);
                var factory = new ImageFactory();
                factory.Load(path)
                    .ToSquare()
                    .Resize(new Size(32, 32))
                    .Save(string.Format(_targetFilesTemplate, index, "l"))
                    .Resize(new Size(16, 16))
                    .Save(string.Format(_targetFilesTemplate, index, "s"))
                    .Reset()
                    .ToHoriz()
                    .Resize(new Size(32, 16))
                    .Save(string.Format(_targetFilesTemplate, index, "h"))
                    .Reset()
                    .ToVert()
                    .Resize(new Size(16, 32))
                    .Save(string.Format(_targetFilesTemplate, index, "v"));

            }
        }
    }
}

public static class ImageFactoryEx
{
    public static ImageFactory ToSquare(this ImageFactory facory)
    {
        int min = Math.Min(facory.Image.Size.Height, facory.Image.Size.Width);
        var rect = new Rectangle((facory.Image.Size.Width - min) / 2, (facory.Image.Size.Height - min) / 2, min, min);
        return facory.Crop(rect);
    }

    public static ImageFactory ToHoriz(this ImageFactory facory)
    {
        var width = facory.Image.Size.Width;
        var height = facory.Image.Size.Height;
        var rect = width/height >= 2
            ? new Rectangle((width - height/2)/2, 0, height*2, height)
            : new Rectangle(0, (height - width/2)/2, width, width/2);
        return facory.Crop(rect);
    }
    public static ImageFactory ToVert(this ImageFactory facory)
    {
        var width = facory.Image.Size.Width;
        var height = facory.Image.Size.Height;
        var rect = height/width >= 2
            ? new Rectangle(0, (height - width/2)/2, width, width*2)
            : new Rectangle((width - height/2)/2, 0, height/2, height);
        return facory.Crop(rect);
    }
} 
