﻿namespace Collage.ImageUploader.Configs
{
    public class ElasticConfig
    {
        public string InputFilesPath { get; set; }
        public string ElasticUrl { get; set; }
        public string Index { get; set; }
    }
}