﻿namespace Collage.ImageUploader.Configs
{
    public class CompressConfig
    {
        public string InputFilesPath { get; set; }
        public string TargetFilesPath { get; set; }
    }
}
