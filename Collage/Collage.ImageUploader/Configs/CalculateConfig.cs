﻿namespace Collage.ImageUploader.Configs
{
    public class CalculateConfig
    {
        public string InputFilesPath { get; set; }
        public string TargetFilesPath { get; set; }
    }
}
