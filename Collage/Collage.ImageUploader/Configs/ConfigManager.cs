﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;

namespace Collage.ImageUploader.Configs
{
    public static class ConfigManager
    {
        private static IConfiguration Config => new ConfigurationBuilder()
            //.SetBasePath(AppContext.BaseDirectory)
            .AddJsonFile("config.json")
            .Build();

        public static TConfig GetConfig<TConfig>() where TConfig : class , new()
        {
                var result = new TConfig();
                Config.GetSection(typeof(TConfig).Name).Bind(result);
                return result;
        }
    }
}