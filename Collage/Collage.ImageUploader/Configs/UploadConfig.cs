﻿namespace Collage.ImageUploader.Configs
{
    public class UploadConfig
    {
        public string InputFilesPath { get; set; }
        public string TargetFilesPath { get; set; }
    }
}
