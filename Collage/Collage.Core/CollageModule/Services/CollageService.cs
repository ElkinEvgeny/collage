﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Collage.Core.CollageModule.Interfaces;
using Collage.Core.Helpers;
using Nest;

namespace Collage.Core.CollageModule.Services
{
    public class CollageService : ICollageService
    {
        public string GetMark(Bitmap bitmap)
        {
            var clasters = PhotoProcessingHelper.Clasters(bitmap);
            var metrics = PhotoProcessingHelper.CalculateMetrics(bitmap, 4);

            return Get(clasters, metrics);
        }

        private static string Get(IEnumerable<int> clasters, List<int> metrics)
        {
            //TODO:: все очень плохо
            var uri = new Uri("http://localhost:9200");
            var client = new ElasticClient(uri);

            foreach (var claster in clasters)
            {
                var response = client.Search<PhotoCharacteristics>(x => x
                    .Query(i => i.Term(t => t.Field(z => z.Claster).Value(claster)))
                    .Index("collage_last")
                    .Take(5000)
                    .AllTypes());
                var result = string.Empty;
                var optimalWeigh = int.MaxValue;
                foreach (var photoCharacteristicse in response.Documents)
                {
                    var error = metrics.Zip(photoCharacteristicse.Metrics, (x, y) => Math.Abs(x - y)).Sum();
                    if (error < optimalWeigh)
                    {
                        optimalWeigh = error;
                        result = photoCharacteristicse.Path;
                    }
                }

                if (result != string.Empty)
                {
                    return "D:/Collage/Storage/" + result;
                }
            }
            return null;
        }

        public Bitmap GetSegment(string mark)
        {
            return new Bitmap(mark, true);
        }
    }

    public class PhotoCharacteristics
    {
        public int Claster { get; set; }

        public int[] Metrics { get; set; }

        public string Path { get; set; }
    }
}
