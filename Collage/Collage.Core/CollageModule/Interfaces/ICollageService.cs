﻿using System.Drawing;

namespace Collage.Core.CollageModule.Interfaces
{
    public interface ICollageService
    {
        string GetMark(Bitmap bitmap);
        Bitmap GetSegment(string mark);
    }
}
