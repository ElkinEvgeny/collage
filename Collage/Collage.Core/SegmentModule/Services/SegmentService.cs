﻿using System.Linq;
using Collage.Core.Helpers;
using Collage.Core.SegmentModule.DomainModels;
using Collage.Core.SegmentModule.Interfaces;
using Collage.Core.SegmentModule.Models;

namespace Collage.Core.SegmentModule.Services
{
    public class SegmentService : ISegmentService
    {
        public Segment[] GetSegments(int totalWidth, int totalHeight)
        {
            var matrixDomainModel = new MatrixDomainModel(totalWidth, totalHeight);

            for (var i = 0; i < totalHeight; i++)
            {
                for (var j = 0; j < totalWidth; j++)
                {
                    var availableSizes = matrixDomainModel.AvailableSegments(j, i);

                    if (availableSizes.Any())
                    {
                        var size = availableSizes.RandomItem();
                        matrixDomainModel.Reserve(j, i, size.Item1, size.Item2);
                    }
                }
            }

            return matrixDomainModel.Segments;
        }
    }
}