﻿namespace Collage.Core.SegmentModule.Models
{
    public class Segment
    {
        public int Top { get; set; }
        public int Left { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
    }
}
