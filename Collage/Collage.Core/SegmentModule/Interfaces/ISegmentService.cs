﻿using Collage.Core.SegmentModule.Models;

namespace Collage.Core.SegmentModule.Interfaces
{
    public interface ISegmentService
    {
        Segment[] GetSegments(int totalWidth, int totalHeight);
    }
}
