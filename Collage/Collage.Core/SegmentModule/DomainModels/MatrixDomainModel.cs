﻿using System;
using System.Collections.Generic;
using System.Linq;
using Collage.Core.SegmentModule.Models;

namespace Collage.Core.SegmentModule.DomainModels
{
    public class MatrixDomainModel
    {
        private readonly int _width;
        private readonly int _height;

        private readonly bool[,] _matrix;
        private readonly IList<Segment> _segments;

        public MatrixDomainModel(int width, int height)
        {
            _width = width;
            _height = height;
            _matrix = new bool[height, width];
            _segments = new List<Segment>();
        }

        public Tuple<int, int>[] AvailableSegments(int x, int y)
        {
            return _availableSegmentSizes.Where(i => IsAvailable(x, y, i.Item1, i.Item2)).ToArray();
        }

        public void Reserve(int x, int y, int width, int height)
        {
            for (var i = y; i < y + height; i++)
            {
                for (var j = x; j < x + width; j++)
                {
                    _matrix[i, j] = true;
                }
            }

            _segments.Add(new Segment { Top = y, Left = x, Width = width, Height = height });
        }

        public Segment[] Segments => _segments.ToArray();

        private bool IsAvailable(int x, int y, int width, int height)
        {
            for (var i = y; i < y + height; i++)
            {
                for (var j = x; j < x + width; j++)
                {
                    if (i == _height || j == _width || _matrix[i, j] == true)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private readonly Tuple<int, int>[] _availableSegmentSizes = { Tuple.Create(32, 32), Tuple.Create(16, 32), Tuple.Create(32, 16), Tuple.Create(16, 16) };
    }
}