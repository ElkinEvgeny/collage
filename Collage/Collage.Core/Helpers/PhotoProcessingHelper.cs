﻿using System;
using System.Collections.Generic;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using System.Drawing;

namespace Collage.Core.Helpers
{
    public static class PhotoProcessingHelper
    {
        public static List<int> CalculateMetrics(Bitmap bitmap, int size)
        {
            var originalImage = new Image<Bgr, byte>(bitmap);
            return Calculate(size, originalImage);
        }

        public static List<int> CalculateMetrics(string file, int size)
        {
            var originalImage = new Image<Bgr, byte>(file);
            return Calculate(size, originalImage);
        }

        private static List<int> Calculate(int size, Image<Bgr, byte> originalImage)
        {
            var hsvImage = new Image<Hsv, byte>(originalImage.Width, originalImage.Height);
            CvInvoke.CvtColor(originalImage, hsvImage, ColorConversion.Bgr2Hsv);
            var segmentsCountVertical = hsvImage.Height / size;
            var segmentsCountHorizontal = hsvImage.Width / size;
            var listSegments = new List<int>();
            for (var i = 0; i < segmentsCountVertical; i++)
            {
                for (var j = 0; j < segmentsCountHorizontal; j++)
                {
                    hsvImage.ROI = new Rectangle(j * size, i * size, size, size);
                    var averageColor = hsvImage.GetAverage();
                    listSegments.Add((int)(averageColor.Hue + 0.5 * averageColor.Satuation + 0.5 * averageColor.Value));
                }
            }
            return listSegments;
        }

        public static int CalculateClaster(Bitmap bitmap)
        {
            var originalImage = new Image<Bgr, byte>(bitmap);
            return Claster(originalImage);
        }

        public static int CalculateClaster(string file)
        {
            var originalImage = new Image<Bgr, byte>(file);
            return Claster(originalImage);
        }

        public static IEnumerable<int> Clasters(Bitmap bitmap)
        {
            var originalImage = new Image<Bgr, byte>(bitmap);
            var average = originalImage.GetAverage();
            yield return CalculateClaster(average.Red, average.Green, average.Blue, originalImage.Width, originalImage.Height);

            yield return CalculateClaster(average.Red +32, average.Green, average.Blue, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red +32, average.Green+32, average.Blue, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red +32, average.Green-32, average.Blue, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red +32, average.Green, average.Blue+32, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red +32, average.Green, average.Blue-32, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red + 32, average.Green + 32, average.Blue+32, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red + 32, average.Green - 32, average.Blue-32, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red + 32, average.Green+32, average.Blue + 32, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red + 32, average.Green-32, average.Blue - 32, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red + 32, average.Green + 32, average.Blue - 32, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red + 32, average.Green - 32, average.Blue + 32, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red + 32, average.Green + 32, average.Blue - 32, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red + 32, average.Green - 32, average.Blue + 32, originalImage.Width, originalImage.Height);

            yield return CalculateClaster(average.Red - 32, average.Green, average.Blue, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red - 32, average.Green + 32, average.Blue, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red - 32, average.Green - 32, average.Blue, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red - 32, average.Green, average.Blue + 32, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red - 32, average.Green, average.Blue - 32, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red - 32, average.Green + 32, average.Blue + 32, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red - 32, average.Green - 32, average.Blue - 32, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red - 32, average.Green + 32, average.Blue + 32, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red - 32, average.Green - 32, average.Blue - 32, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red - 32, average.Green + 32, average.Blue - 32, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red - 32, average.Green - 32, average.Blue + 32, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red - 32, average.Green + 32, average.Blue - 32, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red - 32, average.Green - 32, average.Blue + 32, originalImage.Width, originalImage.Height);

            yield return CalculateClaster(average.Red + 64, average.Green, average.Blue, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red + 64, average.Green + 64, average.Blue, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red + 64, average.Green - 64, average.Blue, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red + 64, average.Green, average.Blue + 64, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red + 64, average.Green, average.Blue - 64, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red + 64, average.Green + 64, average.Blue + 64, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red + 64, average.Green - 64, average.Blue - 64, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red + 64, average.Green + 64, average.Blue + 64, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red + 64, average.Green - 64, average.Blue - 64, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red + 64, average.Green + 64, average.Blue - 64, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red + 64, average.Green - 64, average.Blue + 64, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red + 64, average.Green + 64, average.Blue - 64, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red + 64, average.Green - 64, average.Blue + 64, originalImage.Width, originalImage.Height);

            yield return CalculateClaster(average.Red - 64, average.Green, average.Blue, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red - 64, average.Green + 64, average.Blue, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red - 64, average.Green - 64, average.Blue, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red - 64, average.Green, average.Blue + 64, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red - 64, average.Green, average.Blue - 64, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red - 64, average.Green + 64, average.Blue + 64, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red - 64, average.Green - 64, average.Blue - 64, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red - 64, average.Green + 64, average.Blue + 64, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red - 64, average.Green - 64, average.Blue - 64, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red - 64, average.Green + 64, average.Blue - 64, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red - 64, average.Green - 64, average.Blue + 64, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red - 64, average.Green + 64, average.Blue - 64, originalImage.Width, originalImage.Height);
            yield return CalculateClaster(average.Red - 64, average.Green - 64, average.Blue + 64, originalImage.Width, originalImage.Height);

        }
        
        private static int Claster(Image<Bgr, byte> originalImage)
        {
            var average = originalImage.GetAverage();
            return CalculateClaster(average.Red, average.Green, average.Blue, originalImage.Width, originalImage.Height);
        }

        private static int CalculateClaster(double r, double g, double b, int w, int h)
        {
            var claster = (int)Math.Round(r / 32) + ((int)Math.Round(g / 32) << 4) +
                ((int)Math.Round(b / 32) << 8);
            claster += GetImageTypeClasterValue(w, h);
            return claster;
        }

        private static int GetImageTypeClasterValue(int width, int height)
        {
            if (width > height) return 0;
            if (width < height) return 1 << 12;
            if (width == 32) return 2 << 12;
            if (width == 16) return 3 << 12;
            throw new System.Exception();
        }
    }
}
