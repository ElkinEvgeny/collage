﻿using System;

namespace Collage.Core.Helpers
{
    public static class RandomHelper
    {
        private static readonly Random Random = new Random();

        public static int Next(int max) => Random.Next(max);
    }
}
