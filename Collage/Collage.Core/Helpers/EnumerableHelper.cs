﻿using System;
using System.Linq;

namespace Collage.Core.Helpers
{
    public static class EnumerableHelper
    {
        private static readonly Random Random = new Random();

        public static TItem RandomItem<TItem>(this TItem[] items) => items[Random.Next(items.Length)];

        public static TItem[] Shuffle<TItem>(this TItem[] items) => items.OrderBy(x => RandomHelper.Next(9999)).ToArray();
    }
}
